/*Using DOM

	before, we are using the following codes in terms of selecting elements inside the html file

*/
// Document refers ti the whole webpage and the "getElementById" selects the element with the same id as its arguments
/*document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name');
document.getElementTagName('input');*/

// querySelector replaces the three "getElement" selectors and makes use if css format in terms  of selecting the elements inside teh html arguments(#-id, .-class, tagName - tag)
document.querySelector("#txt-first-name")

// Event Listener
// events are all interaction of the user to our webpage; such examples are clicking , hovering, reloading, keypressing/keyup(typing);
const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

function addFullName(txtFirstName, txtLastName){
	return txtFirstName.value + txtLastName.value
}

let fullName = addFullName(txtFirstName, txtLastName)
console.log(fullName)
// fullName.addEventListener("keyup", (event) =>{
	
// 	spanFullName.innerHTML = fullName;
// });

