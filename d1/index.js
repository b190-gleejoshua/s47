/*Using DOM

	before, we are using the following codes in terms of selecting elements inside the html file

*/
// Document refers ti the whole webpage and the "getElementById" selects the element with the same id as its arguments
/*document.getElementById('txt-first-name');
document.getElementByClassName('txt-last-name');
document.getElementTagName('input');*/

// querySelector replaces the three "getElement" selectors and makes use if css format in terms  of selecting the elements inside teh html arguments(#-id, .-class, tagName - tag)
document.querySelector("#txt-first-name")

// Event Listener
// events are all interaction of the user to our webpage; such examples are clicking , hovering, reloading, keypressing/keyup(typing);
const txtFirstName = document.querySelector("#txt-first-name")
const txtLastName = document.querySelector("#txt-last-name")
const spanFullName = document.querySelector("#span-full-name")

// to perform an action when an event triggers, first you have to listen to it

/*
	the funciton used is the addEventListener - allows a block of codes ti listen to an event for the to be executed
		addEventListener - takes two arguments: 
		1 - string that identifies the event to which the codes will listen; 
		2- function that the listener will execute once the specified event is triggered 
*/
txtFirstName.addEventListener("keyup", (event) =>{
	// .innerHTML - allows the element to recored/duplicate the value of the selected variable (txtFirstName.value);
	// .value is needed since without it, the .innerHTML will only record that type of element the target variable is inside the HTML document insted of getting its value
	spanFullName.innerHTML = txtFirstName.value;
});

// multipple listeners can also be assigned to same event; in the same way, same listeners can listen to different events
txtFirstName.addEventListener("keyup", (event) =>{
	// event - contains the information the triggered event passed form the first argmument
	// event.target - contains the element where the event happened
	// event.target.value - gets the value of the input object where the event happened
	console.log(event.target);
	console.log(event.target.value);
});

txtLastName.addEventListener("keyup", (event) =>{
	spanFullName.innerHTML = txtLastName.value;
});


